'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.addColumn(
      'Campaigns',
      'withdrawalCount',
      Sequelize.INTEGER
    );
    await queryInterface.addColumn(
      'Campaigns',
      'updateCount',
      Sequelize.INTEGER
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.removeColumn('Campaigns', 'withdrawalCount');
    await queryInterface.removeColumn('Campaigns', 'updateCount');
  },
};
