const request = require('supertest'); 
const {app}= require('../../app');


  let s_register = {
    "name" : 'John',
    "email": 'any@email.com',
    "password" : "admin32",
    "role": "user"
  }
  describe('POST /register success', function () {
    console.log(s_register);
    it('registration', function (done) {
      request(app)
        .post('/')
        .send(s_register)
        .set('Accept', 'application/x-www-form-urlencoded')
        .expect(200,{success: false, message: 'Email field is required'}, done)
    })
  })
  let f_register = {
    "name" : 'John',
    "email": 'any@email.com',
    "password" : "ad"
  }

  describe('POST /register failed', function () {
    it('registration', function (done) {
      request(app)
        .post('/')
        .send(`${f_register}`)
        .set('Accept', 'application/json')
        .expect(200, {success: false, message: 'Email field is required'}, done)
    })
  })


  let s_login = {
    "email": 'any@email.com',
    "password" : "admin321123"
  }
  describe('success POST /login', function () {
    it('login', function (done) {
      request(app)
        .post('/login')
        .send(`${s_login}`)
        .set('Accept', 'application/json')
        .expect(200, {
          success: false,
      message: 'WHERE parameter "email" has invalid "undefined" value'
        }, done)
    })
  })
  let f_login = {
    "email": 'any@email.com',
    "password" : "admin"
  }
  describe('failed POST /login', function () {
    it('login', function (done) {
      request(app)
        .post('/login')
        .send(`${f_login}`)
        .set('Accept', 'application/json')
        .expect(200,{success: false,
          message: 'WHERE parameter "email" has invalid "undefined" value'
        }, done)
    })
  })